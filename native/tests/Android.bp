// Copyright 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_android_rubidium",
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "packages_modules_ExtServices_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-Apache-2.0
    default_applicable_licenses: ["packages_modules_ExtServices_license"],
}

filegroup {
    name: "libextservices_test_default_map",
    srcs: ["libextservices_test_default.map"],
}

cc_defaults {
    name: "libextservices_test_default",
    stl: "libc++_static",
    version_script: ":libextservices_test_default_map",
}

// Run this test using "atest libextservices_test -- --all-abi" or it will fail to run on 64 bit
// devices.
cc_test {
    name: "libextservices_test-tplus",
    defaults: ["libextservices_test_default"],
    test_suites: [
        "general-tests",
        "mts-extservices",
    ],

    shared_libs: [
        "libandroid",
        "liblog",
    ],
    static_libs: [
        "libbase",
        "libextservices",
        "libfft2d",
        "libgmock_main",
        "libgmock",
        "libgtest",
    ],

    cflags: [
        "-Wall",
        "-Werror",
    ],

    srcs: [
        "*.cpp",
    ],

    data: [
        "test_data/*.raw",
    ],
    compile_multilib: "prefer32",
    min_sdk_version: "33",
    test_config: "AndroidTest-tplus.xml",
}

// Run this test using "atest libextservices_test -- --all-abi" or it will fail to run on 64 bit
// devices.
cc_test {
    name: "libextservices_test-sminus",
    defaults: ["libextservices_test_default"],
    test_suites: [
        "general-tests",
        "mts-extservices",
    ],

    shared_libs: [
        "libandroid",
        "liblog",
    ],
    static_libs: [
        "libbase",
        "libextservices",
        "libfft2d",
        "libgmock_main",
        "libgmock",
        "libgtest",
    ],

    cflags: [
        "-Wall",
        "-Werror",
    ],

    srcs: [
        "*.cpp",
    ],

    data: [
        "test_data/*.raw",
    ],
    compile_multilib: "prefer32",
    min_sdk_version: "30",
    test_config: "AndroidTest-sminus.xml",
}
